#!/bin/sh

# Extract IP addresses from list
tail -n +2 list.xml | grep -F -v -f ignorehosts.txt | awk -F ';' '{print $1}' | sed 's/ | /\n/g' | cat - customips.txt | sort -u | grep -F -v -f ignoreips.txt > iplist.txt
tail -n +2 list.xml | grep -F -v -f ignorehosts.txt | awk -F ';' '{if (($2 == "" && $3 == "") || ($1 == $2)) {print $1}}' | sed 's/ | /\n/g' | cat - customips.txt | sort -u | grep -F -v -f ignoreips.txt > iplist_blockedbyip.txt
echo -n "Unique IPs in list: "
cat iplist.txt | wc -l

